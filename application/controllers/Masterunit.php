<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterunit extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->model('M_Unit','',true);
		$this->redirect->backToCurrentUser();	
		$this->redirect->backToCurrentSek();
		$this->redirect->backToLogin();

		$sesi = $_SESSION['logged_in'];
		$this->data = array(
				'session' => $sesi,
				'masterunit' => 'active'
			);
		$_SESSION['referred_from'] = current_url();
	}

	public function index()
	{
		
		$this->data['unit']=$this->M_Unit->getUnit();				
		// die(var_dump($this->data));
		$this->load->view('view_master_unit',$this->data);

		// var_dump($data);
		
	}

	public function tambah()
	{
		$this->load->view('form_unit',$this->data);

		
	}

	public function prosesform($id=null){
		
		$this->form_validation->set_error_delimiters('<div class="text-red">','</div>');
		$this->form_validation->set_rules('nama_unit', 'Nama Unit', 'trim|required|max_length[255]'
			,array('trim'=>'','required'=>'Kolom {field} harus diisi.','max_length'=> 'Kolom %s maksimal 255 karakter'));
		
		if ($this->form_validation->run() == FALSE) {
			if ($id!=null) {
				$this->data['id_unit'] = $id;
			}
			$this->load->view('form_unit',$this->data);
		} else {
			$nama_unit = $this->input->post('nama_unit',true);
			if ($id==null) {
				$data = array(
					'nama_unit' => $nama_unit
					);
				$this->M_Unit->setUnit($data);
				echo "<script>alert('Data Unit baru telah ditambahkan');</script>";

				redirect('masterunit','refresh');
			}else{
				$data = array(
						'id_unit' => $id,
						'nama_unit' => $nama_unit
					);
				$this->M_Unit->setUnit($data);
				echo "<script>alert('Data Unit telah diperbaharui');</script>";

				redirect('masterunit','refresh');
			}
		}
	}

	public function edit($id)
	{
		$this->data['id_unit'] = $id;
		$this->data['dataunit'] = $this->M_Unit->getIDUnit($id);
		
		$this->load->view('form_unit',$this->data);

		
	}

	public function hapus($id)
	{
		$this->M_Unit->deleteUnit($id);
		echo "<script>alert('Data Unit telah terhapus');</script>";

		redirect('masterunit','refresh');
	}
}

/* End of file KategoriBarang.php */
/* Location: ./application/controllers/KategoriBarang.php */