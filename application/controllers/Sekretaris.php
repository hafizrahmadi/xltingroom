<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sekretaris extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->library('email'); // load email library
		$this->load->model('M_Booking','',true);
		$this->redirect->backToCurrentUser();		
		$this->redirect->backToCurrentAdmin();
		$this->redirect->backToLogin();

		$sesi = $_SESSION['logged_in'];
		
		$this->data = array(
				'session' => $sesi,
				'demandbooking' => 'active'
			);
		$this->data['notifDemand'] = $this->M_Booking->getUnreadDemandBook($this->data['session']['id_lantai']);
		$this->data['notifConfirm'] = $this->M_Booking->getUnreadConfirmBook($this->data['session']['id_lantai']);
		$_SESSION['referred_from'] = current_url();
	}

	public function index()
	{
		$this->demandbooking();
	}

	public function demandbooking()
	{
		$id_lantai = $this->data['session']['id_lantai'];
		$this->data['booking'] = $this->M_Booking->getBookingDemand($id_lantai);
		
		// die(var_dump($this->data));
		$this->load->view('view_demand_booking',$this->data);		
	}

	public function confirmbooking()
	{
		$id_lantai = $this->data['session']['id_lantai'];
		$this->data['booking'] = $this->M_Booking->getBookingApproved($id_lantai);
		
		// die(var_dump($this->data));
		$this->load->view('view_confirm_booking',$this->data);		
	}

	public function approve($id_booking)
	{
			$this->M_Booking->approve($id_booking);
			$databook = $this->M_Booking->getIDBooking($id_booking);

	        	$htmlContent = "<p>Good Morning ".$databook[0]['nama']." ! <br><br>";
				$htmlContent .= "Thank you for using <b>TINGROOM</b>, <br>";
				$htmlContent .= "We would like to inform you that secretary have been <b>APPROVED</b> your request for <b>".
				strtoupper($databook[0]['nama_ruangan'])."</b> at ".$databook[0]['jam_jadwal'].
				" on ".date('d M Y',strtotime($databook[0]['waktu']))." in ".ucwords($databook[0]['nama_gedung'])." Floor ".
				$databook[0]['nama_lantai'].". <br><br>";
				$htmlContent .= "Feel free to use your meeting room and keep the room clean always. <br><br>";				
				$htmlContent .= "Regards, <br><br>
				<img src=".base_url('assets/img/icons/icon-tingroom-chair-s.png')." style='width:100px; height:100px;'><br><br> TINGROOM TEAM</p>";

	            $this->email->from('admtingroom@gmail.com', 'Admin Tingroom');
	            $this->email->to($databook[0]['email']);

	            $this->email->subject('Notifikasi Tingroom : Accept Pengajuan Booking Ruangan Meeting');
	            $this->email->message($htmlContent);
	           	$this->email->send();
	  
			echo "<script>
					alert('Data booking telah di-approve.');
					document.location='".site_url('sekretaris/')."';
				</script>";
	}

	public function reject($id_booking)
	{
			$this->M_Booking->reject($id_booking);
			

	        	$databook = $this->M_Booking->getIDBooking($id_booking);

	        	$htmlContent = "<p>Good Morning ".$databook[0]['nama']." ! <br><br>";
				$htmlContent .= "Thank you for using <b>TINGROOM</b>, <br>";
				$htmlContent .= "We would like to inform you that secretary have been <b>REJECTED</b> your request for <b>".
				strtoupper($databook[0]['nama_ruangan'])."</b> at ".$databook[0]['jam_jadwal'].
				" on ".date('d M Y',strtotime($databook[0]['waktu']))." in ".ucwords($databook[0]['nama_gedung'])." Floor ".
				$databook[0]['nama_lantai'].". <br><br>";
				$htmlContent .= "Feel free to use your meeting room and keep the room clean always. <br><br>";				
				$htmlContent .= "Regards, <br><br>
				<img src=".base_url('assets/img/icons/icon-tingroom-chair-s.png')." style='width:100px; height:100px;'><br><br> TINGROOM TEAM</p>";

	            $this->email->from('admtingroom@gmail.com', 'Admin Tingroom');
	            $this->email->to($databook[0]['email']);

	            $this->email->subject('Notifikasi Tingroom : Reject Pengajuan Booking Ruangan Meeting');
	            $this->email->message($htmlContent);
	           	$this->email->send();
	         
			echo "<script>
					alert('Data booking telah di-reject.');
					document.location='".site_url('sekretaris/')."';
				</script>";
	}

	public function confirm($id_booking)
	{
			$this->M_Booking->confirm($id_booking);
			$databook = $this->M_Booking->getIDBooking($id_booking);

	        	$htmlContent = "<p>Good Morning ".$databook[0]['nama']." ! <br><br>";
				$htmlContent .= "Thank you for using <b>TINGROOM</b>, <br>";
				$htmlContent .= "We would like to inform you that secretary have been <b>CONFIRMED</b> your request for <b>".
				strtoupper($databook[0]['nama_ruangan'])."</b> at ".$databook[0]['jam_jadwal'].
				" on ".date('d M Y',strtotime($databook[0]['waktu']))." in ".ucwords($databook[0]['nama_gedung'])." Floor ".
				$databook[0]['nama_lantai'].". <br><br>";
				$htmlContent .= "Feel free to use your meeting room and keep the room clean always. <br><br>";				
				$htmlContent .= "Regards, <br><br>
				<img src=".base_url('assets/img/icons/icon-tingroom-chair-s.png')." style='width:100px; height:100px;'><br><br> TINGROOM TEAM</p>";

	            $this->email->from('admtingroom@gmail.com', 'Admin Tingroom');
	            $this->email->to($databook[0]['email']);

	            $this->email->subject('Notifikasi Tingroom : Confirm Pengajuan Booking Ruangan Meeting');
	            $this->email->message($htmlContent);
	           	$this->email->send();

			echo "<script>
					alert('Data booking telah di-confirm.');
					document.location='".site_url('sekretaris/confirmbooking')."';
				</script>";
	}

	public function riwayatbooking(){
		$id_lantai = $this->data['session']['id_lantai'];
		$this->data['booking'] = $this->M_Booking->getHistoryBooking($id_lantai);

		$this->load->view('view_riwayat_booking',$this->data);
	}

}

/* End of file KategoriBarang.php */
/* Location: ./application/controllers/KategoriBarang.php */