<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apps extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->model('M_Lantai','',true);
		$this->load->model('M_Ruangan','',true);
		$this->load->model('M_Jadwal','',true);
		$this->load->model('M_Booking','',true);
		$this->load->model('M_User','',true);
		$this->redirect->backToCurrentAdmin();
		$this->redirect->backToLogin();
		$this->data['session'] = $_SESSION['logged_in'];
		$this->data['apps'] = 'active';
		$this->data['notifUnread'] = $this->M_Booking->getUnreadBookUser($this->data['session']['id_user']);
		$this->data['user'] = $this->M_User->getIDUser($this->data['session']['id_user']);
		$_SESSION['referred_from'] = current_url();
	}	

	public function index()
	{		
		$this->data['lantai'] = $this->M_Lantai->getLantai();
		$this->load->view('view_landing_page',$this->data);
	}

	public function book()
	{		
		$this->data['lantai'] = $this->M_Lantai->getLantai();
		$this->load->view('new_view_booking1',$this->data);
	}

	public function booking(){
		if ($this->input->post('id_lantai')!=null) {
			$_SESSION['id_lantai'] = $this->input->post('id_lantai');
		}
		
		if(!isset($_SESSION['id_lantai'])){
		    redirect('Apps/book');
		}
		
		if ($this->input->post('waktu')!=null) {
			$_SESSION['waktu_booking'] = date('Y-m-d',strtotime($this->input->post('waktu')));
		}
		
		if(!isset($_SESSION['waktu_booking'])){
		    redirect('Apps/book');
		}

		$id_lantai = $_SESSION['id_lantai'];
		$waktu = $_SESSION['waktu_booking'];
		$this->data['lantai'] = $this->M_Lantai->getIDLantai($id_lantai);
		$this->data['ruangan'] = $this->M_Ruangan->getRuanganInLantai($id_lantai);
		$this->data['waktu'] = $waktu;
		$this->data['jadwal'] = $this->M_Jadwal->getJadwalInLantai($id_lantai,$waktu);
		
		$this->load->view('new_view_booking2',$this->data);
		
	}

	public function book_demand(){
		if ($this->input->post('id_ruangan')!=null) {
			$_SESSION['id_ruangan'] = $this->input->post('id_ruangan');
		}
		
		if(!isset($_SESSION['id_ruangan'])){
		    redirect('Apps/book');
		}

		if ($this->input->post('jadwal')!=null) {
			$_SESSION['jadwal'] = $this->input->post('jadwal');
		}
		
		if(!isset($_SESSION['jadwal'])){
		    redirect('Apps/book');
		}
		
		$id_ruangan = $_SESSION['id_ruangan'];
		$jadwal = $_SESSION['jadwal'];
		$this->data['waktu'] = $_SESSION['waktu_booking'];
		$this->data['ruangan'] = $this->M_Ruangan->getIDRuangan($id_ruangan);
		$this->data['jadwal'] = $this->M_Jadwal->getIDArrJadwal($jadwal);
		
		$this->load->view('new_form_booking',$this->data);
	}

	public function proses_book(){
		
		$this->data['id_user'] = $this->data['session']['id_user'];
		$phptime = strtotime($this->input->post('waktu'));
		$this->data['waktu'] = date('Y-m-d', $phptime);
		$this->data['jadwal'] = $this->input->post('jadwal');
		$this->data['deskripsi'] = $this->input->post('deskripsi');
		
		$this->M_Booking->setBooking($this->data);
		echo "<script>
					alert('Data booking telah ditambahkan. Tunggu konfirmasi dari sekretaris.');
					document.location='".site_url('history/')."';
				</script>";
	}
}

