<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->model('M_User','',true);
		$this->load->model('M_Booking','',true);
		$this->redirect->backToCurrentAdmin();
		$this->redirect->backToLogin();
		$_SESSION['referred_from'] = current_url();
		$this->data['session'] = $_SESSION['logged_in'];
		$this->data['history'] = 'active';
		$this->data['notifUnread'] = $this->M_Booking->getUnreadBookUser($this->data['session']['id_user']);
		$this->data['user'] = $this->M_User->getIDUser($this->data['session']['id_user']);
	}	

	public function index()
	{	
		$id_user = $this->data['session']['id_user'];
		$this->data['inprogress'] = 'active';
		$this->data['booking'] = $this->M_Booking->getBookDemand($id_user);
		$this->data['det_booking'] = $this->M_Booking->getDetBookDemand($id_user);

		$this->M_Booking->updateUnreadBookPro($id_user);
		$this->load->view('new_view_history1',$this->data);		
		
	}

	public function completed(){

		$id_user = $this->data['session']['id_user'];
		$this->data['completed'] = 'active';
		$this->data['booking'] = $this->M_Booking->getBookCompleted($id_user);
		$this->data['det_booking'] = $this->M_Booking->getDetBookCompleted($id_user);
		
		$this->M_Booking->updateUnreadBookCom($id_user);
		$this->load->view('new_view_history2',$this->data);		
	}
	
}