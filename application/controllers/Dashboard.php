<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->model('M_Booking','',true);
		$this->redirect->backToCurrentUser();		
		$this->redirect->backToLogin();

		$sesi = $_SESSION['logged_in'];
		$this->data = array(
			'session' => $sesi			
			);
		if ($this->data['session']['level']==2) {
			$this->data['notifDemand'] = $this->M_Booking->getUnreadDemandBook($this->data['session']['id_lantai']);
			$this->data['notifConfirm'] = $this->M_Booking->getUnreadConfirmBook($this->data['session']['id_lantai']);
		}
		
	}	

	public function index()
	{		
		
		$this->data['home'] = 'active';
		$this->load->view('view_dashboard1',$this->data);
		
		$_SESSION['referred_from'] = current_url();
	}

	

}
