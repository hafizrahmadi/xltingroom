<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Redirect');
		$this->load->model('M_Booking','',true);
		$this->load->model('M_User','',true);

		$this->redirect->backToCurrentAdmin();
		$this->redirect->backToLogin();
		$_SESSION['referred_from'] = current_url();
		$this->data['session'] = $_SESSION['logged_in'];
		$this->data['setting'] = 'active';
		$this->data['notifUnread'] = $this->M_Booking->getUnreadBookUser($this->data['session']['id_user']);
		$this->data['user'] = $this->M_User->getIDUser($this->data['session']['id_user']);
	}	

	public function index()
	{		
		$this->data['setfaq'] = 'active';		
		$this->load->view('view_faq',$this->data);
		
		
	}

	public function toc(){
		$this->data['settoc'] = 'active';
		$this->load->view('view_toc',$this->data);
	}
}