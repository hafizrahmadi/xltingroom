<?php $this->load->view('template/new_head_frontend') ?>
			<p style="font-family: 'musseosans-100'">Please choose your room and let's collaborate!</p>
	      	<div class="box-dgrey"><?php echo ucwords($lantai[0]['nama_gedung'])." <span class='col-lantai'>".$lantai[0]['nama_lantai']."</span>" ; ?> - For <b style="color:#f39c12;"><?php echo date('d M Y',strtotime($waktu)) ?></b></div>
	      	<div class="content-booking-2">
			<form action="<?php echo site_url('apps/book_demand') ?>" method="post">
			<?php 
	            $no_ruang = 1;
	            foreach ($ruangan as $value) {  
	              $no_jadwal = 1;
          	?>				
	      		<div class="col-xs-12 box-green arrow-down" data-toggle="collapse" data-target="#collapseDiv<?php echo $no_ruang; ?>" >
	            	<div class="col-xs-3 t-bold"><?php echo $value['nama_ruangan'] ?></div>
	            	<div class="col-xs-3 t-white i-board"><?php echo ($value['board']<2?$value['board'].' Unit':$value['board'].' Units') ?></div>
	            	<div class="col-xs-3 t-white i-projector"><?php echo $value['proyektor']==0?"None":"Available" ?></div>
	            	<div class="col-xs-3 t-white i-chair"><?php echo $value['kapasitas'] ?> person</div>
	          	</div>
	          	<div class="col-xs-12 collapse" id="collapseDiv<?php echo $no_ruang; ?>" value="<?php echo $value['id_ruangan']; ?>">
	            	<div class="funkyradio">
	            	<?php
	            	
		                foreach ($jadwal as $key) {

		                  if ($value['id_ruangan']==$key['id_ruangan']) {
		                  	if ($key['jam_awal']=='12:00') {
		                ?>
		                <div class="col-md-12 col-xs-12 lunchtime"><span><b>Lunch Time</b> (<?php echo $key['jam_awal'].' - 13:00'; ?>)</span> <img src="<?php echo base_url('assets/img/icons/icon-lunch-time.png') ?>" height="26px" width="26px"></div>
		            <?php
		                  	}else if( $key['jam_awal']=='12:30'){
		                  	    continue;
		                  	}else{


		            ?>
		              		<div class="funkyradio-default col-md-4 col-xs-4 <?php echo ($key['status_reserve']>0||$key['status_demand']>0)?"reserved":null ?> <?php echo $key['status_reserve']>0?'res':null; echo $key['status_demand']?'dem':null; ?>">
		                  		<input type="checkbox" name="jadwal[]" value="<?php echo $key['id_jadwal'] ?>" id="checkbox<?php echo $no_ruang.'_'.$no_jadwal ?>"  />
		                  		<label for="checkbox<?php echo $no_ruang.'_'.$no_jadwal ?>">
		                  			<?php 
		                  			$color='';
					                      if ($key['status_demand']>0) {
					                      	$color='#1a44e6';
					                        echo "<span style='color:#1a44e6'>On Demand</span>";
					                        echo "<span style='color:#1a44e6'>By ".$key['nama_demand']."</span>";
					                      }else if($key['status_reserve']>0){
					                      	$color='#f3850e';
					                        echo "<span style='color:#f3850e'>Reserved</span>";
					                        echo "<span style='color:#f3850e'>By ".$key['nama_reserve']."</span>";
					                      }
					                    ?>
		                  			<span style='color:<?php echo $color ?>'><?php echo $key['jam_awal'].' - '.$key['jam_akhir'] ?></span>
		                  			<?php 
		                  				if ($key['status_demand']==0&&$key['status_reserve']==0) {
		                  					echo "<span>Available</span>";
		                  				}
		                  			 ?>
		                  		</label>
		                  	</div>
	                  	<?php
			                    
			                  	}
			                  }
			                  $no_jadwal++;  
			                }
			              ?>
			              </div>
			          </div>
			            <?php
			                $no_ruang++;
			              }
			             ?>          
	              	</div>
	            

	            
	            <div class="col-xs-12 no-padding container-submit-booking" >
          			<span>
              				<input type="hidden" name="id_ruangan" value="">
              				<button class="btn  btn-block btn-orange" type="submit" data-href="#">Continue</button>
            			</form>
             
          			</span>
        		</div>
        		</div>
			</div>
		</div>
<?php $this->load->view('template/new_foot_frontend') ?>