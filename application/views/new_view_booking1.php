<?php $this->load->view('template/new_head_frontend') ?>
	      	<div class="col-xs-12">
				<p style="font-family: 'musseosans-100'">Tingroom helps you reserve a meeting room effectively to collaborate with team! This platform designed with friendly user interface.</p>
				<form id="form-lantai" method="post" action="<?php echo site_url('apps/booking/'); ?>">
			  	<div class="col-xs-12 no-padding">
					<div class="form-group form-date">
				        <div class='input-group date'>
				            <input type="text" id="datepicker" class="form-control datepicker" placeholder="Pilih Tanggal Booking (dd/mm/yyyy)" name="waktu" /> 
				            <span class="input-group-addon btn btn-green"><i class="fa fa-calendar fa-fw "></i> </span>
				        </div>
				    </div>
				</div>
				<div class="col-xs-12 no-padding" style="text-align: center; font-family:'musseosans-500';" >
				   	<?php foreach ($lantai as $key) { ?>
				    		<div data-hre="<?php  echo $key['id_lantai']; ?>" class="building <?php echo ($key['id_lantai']!=$user[0]['id_lantai'])?'building-disabled':null; ?>">
				    				<?php echo ucwords($key['nama_gedung']); ?>
								<div class="floor"><?php echo $key['nama_lantai'];  ?></div>
							</div>

				      	<?php } ?>
				    	
				  		<input type="hidden" name="id_lantai" value="">
					
				</div>
				</form>
			</div>
		</div>
<?php $this->load->view('template/new_foot_frontend') ?>