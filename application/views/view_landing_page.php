<!DOCTYPE html>
<html>
<head>
  <title>Tingroom - Landing Page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--<script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js') ?>"></script>-->
    <script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
    <link href="<?php echo base_url('assets/css/jquery.mobile.datepicker.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/jquery.mobile.datepicker.theme.css') ?>" rel="stylesheet" type="text/css" />

  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/tingroom.css') ?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet" media="screen">
    <link rel="icon" href="<?php echo base_url('assets/img/icons/favico-no-box.png') ?>">


    <style type="text/css">
      .link-landing {
        height: auto;
        width: 100%;
        align-content: center;
        height: 300px;
        background-size: 150px;
        background-repeat: no-repeat;
        background-position: center;
      }
    </style>
</head>
<body>
  <div class="container">
  <!-- <div class="row"> 
       <div class="col-md-12 col-xs-12" style="height: 60px">
       </div>
       
  </div> -->
  <div class="row">
      <div class="col-md-4 col-xs-6">
        <a href="<?php  echo site_url('Apps/book') ?>">
        <div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tingroom.png') ?>') "></div></a>
      </div>
      <div class="col-md-4 col-xs-6">
        <a href="#"><div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tinghelp.png') ?>') "></div></a>
      </div>
      <div class="col-md-4 col-xs-6">
        <a href="#"><div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tingfood.png') ?>') "></div></a>
      </div>
      <div class="col-md-4 col-xs-6">
        <a href="#"><div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tingnews.png') ?>') "></div></a>
      </div>
      <div class="col-md-4 col-xs-6">
        <a href="#"><div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tingcar.png') ?>') "></div></a>
      </div>
      <div class="col-md-4 col-xs-6">
        <a href="#"><div class="link-landing" style="background-image: url('<?php echo base_url('assets/img/icons/icon-lp-tingtraffic.png') ?>') "></div></a>
      </div>    
    </div>
    
  </div>
</body>
</html>