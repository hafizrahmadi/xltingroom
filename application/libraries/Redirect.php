<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redirect{
	public function backToLogin()
	{	
		$session = isset($_SESSION['logged_in'])?$_SESSION['logged_in']:null;
		if (!isset($session)) {
			redirect('Auth','refresh');			
		}else{
			return false;
		}
	}

	public function backToCurrent(){
		$session = isset($_SESSION['logged_in'])?$_SESSION['logged_in']:null;
		if (isset($session)) {
			$referred_from = isset($_SESSION['referred_from'])?$_SESSION['referred_from']:null;
			redirect($referred_from, 'refresh');
		}else{
			return false;
		}
	}

	public function backToCurrentUser(){
		$session = isset($_SESSION['logged_in'])?$_SESSION['logged_in']:null;
		if (isset($session)&&$session['level']==3) {
			$referred_from = isset($_SESSION['referred_from'])?$_SESSION['referred_from']:null;
			redirect($referred_from, 'refresh');
		}else{
			return false;
		}
	}

	public function backToCurrentAdmin(){
		$session = isset($_SESSION['logged_in'])?$_SESSION['logged_in']:null;
		if (isset($session)&&$session['level']==1) {
			$referred_from = isset($_SESSION['referred_from'])?$_SESSION['referred_from']:null;
			redirect($referred_from, 'refresh');
		}else{
			return false;
		}
	}

	public function backToCurrentSek(){
		$session = isset($_SESSION['logged_in'])?$_SESSION['logged_in']:null;
		if (isset($session)&&$session['level']==2) {
			$referred_from = isset($_SESSION['referred_from'])?$_SESSION['referred_from']:null;
			redirect($referred_from, 'refresh');
		}else{
			return false;
		}
	}
}

/* End of file Redirect.php */
/* Location: ./application/controllers/Redirect.php */